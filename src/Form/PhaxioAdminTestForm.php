<?php

namespace Drupal\phaxio\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\phaxio\Services\Fax;

/**
 * Form to send test SMS messages.
 */
class PhaxioAdminTestForm extends FormBase {

  /**
   * Injected Phaxio service Fax class.
   *
   * @var \Drupal\phaxio\Services\Fax
   */
  private $fax;

  /**
   * {@inheritdoc}
   */
  final public function __construct(Fax $fax) {
    $this->fax = $fax;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $fax = $container->get('phaxio.fax');
    return new static($fax);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'phaxio_admin_test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['number'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Phone Number'),
      '#description' => $this->t('The number to send a test fax to.'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send Fax'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $value = $form_state->getValue(['number']);
    if (!is_numeric($value)) {
      $form_state->setErrorByName('number', $this->t('You must enter a phone number'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $number = '+' . $form_state->getValue(['number']);

    $this->fax->send($number, 'https://africau.edu/images/default/sample.pdf');
    $this->messenger()->addStatus($this->t('Attempted to send fax. Check your receiving device.'));
  }

}
