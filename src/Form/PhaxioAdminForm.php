<?php

namespace Drupal\phaxio\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Admin form for Phaxio config.
 */
class PhaxioAdminForm extends ConfigFormBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   */
  final public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $moduleHandler) {
    parent::__construct($config_factory);
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'phaxio_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('phaxio.settings');

    foreach (Element::children($form) as $variable) {
      devel_debug($variable);
      devel_debug($form_state->getValue($form[$variable]['#parents']));
      $config->set($variable, $form_state->getValue($form[$variable]['#parents']));
    }
    $config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['phaxio.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('phaxio.settings');
    $key_exists = $this->moduleHandler->moduleExists('key');

    if ($key_exists) {
      $form['key'] = [
        '#type' => 'key_select',
        '#required' => TRUE,
        '#default_value' => $config->get('key'),
        '#title' => $this->t('Phaxio API key'),
      ];
      $form['secret'] = [
        '#type' => 'key_select',
        '#required' => TRUE,
        '#default_value' => $config->get('secret'),
        '#title' => $this->t('Phaxio authentication secret'),
      ];
    }
    else {
      $form['key'] = [
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' => $this->t('Phaxio API key'),
        '#default_value' => $config->get('key'),
        '#description' => $this->t('Enter your Phaxio API key'),
      ];
      $form['secret'] = [
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' => $this->t('Phaxio Auth Secret'),
        '#default_value' => $config->get('secret'),
        '#description' => $this->t('Enter your Phaxio secret'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

}
