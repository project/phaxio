<?php

namespace Drupal\phaxio\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Default controller for the phaxio module.
 */
class PhaxioController extends ControllerBase {

  /**
   * Handle incoming status updates.
   */
  public function receiveStatus() {
    if (!empty($_REQUEST['fax'])) {
      $this->moduleHandler()->invokeAll('phaxio_status', $_REQUEST);
    }
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Phaxio status'),
    ];
  }

}
