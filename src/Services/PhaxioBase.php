<?php

namespace Drupal\phaxio\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Cache\CacheFactoryInterface;

/**
 * Service class for Phaxio API commands.
 */
class PhaxioBase {

  /**
   * Phaxio API key.
   *
   * @var string
   */
  protected $key;

  /**
   * Phaxio auth secret.
   *
   * @var string
   */
  protected $secret;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The cache factory service.
   *
   * @var \Drupal\Core\Cache\CacheFactoryInterface
   */
  protected $cacheFactory;

  /**
   * Phaxio client.
   *
   * @var \Phaxio
   */
  protected $phaxio;

  /**
   * Phone number.
   *
   * @var string
   */
  private $number;

  /**
   * Initialize properties.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $moduleHandler, CacheFactoryInterface $cacheFactory) {
    $this->configFactory = $config_factory;
    $this->moduleHandler = $moduleHandler;
    $this->cacheFactory = $cacheFactory;
    $this->key = $this->getKey();
    $this->secret = $this->getSecret();
  }

  /**
   * Get the Phaxio client.
   */
  protected function client() {
    if (empty($this->phaxio)) {
      $this->phaxio = new \Phaxio($this->key, $this->secret);
    }
    return $this->phaxio;
  }

  /**
   * Get config/key value.
   */
  private function getConfig(string $key):string {
    $value = $this->configFactory
      ->get('phaxio.settings')
      ->get($key);
    if ($value && $this->moduleHandler->moduleExists('key')) {
      // @phpstan-ignore-next-line
      $key = \Drupal::service('key.repository')->getKey($value);
      if ($key && $key->getKeyValue()) {
        $value = $key->getKeyValue();
      }
    }
    return $value ?? '';
  }

  /**
   * Get the Phaxio API key.
   *
   * @return string
   *   The configured Phaxio API key.
   */
  public function getKey():string {
    if (empty($this->key)) {
      $this->key = $this->getConfig('key');
    }
    return $this->key;
  }

  /**
   * Get the Phaxio Auth Secret.
   *
   * @return string
   *   The configured Phaxio Auth Secret.
   */
  public function getSecret():string {
    if (empty($this->secret)) {
      $this->secret = $this->getConfig('secret');
    }
    return $this->secret;
  }

}
