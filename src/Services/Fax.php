<?php

namespace Drupal\phaxio\Services;

use Drupal\Component\Utility\UrlHelper;

/**
 * Service class for Phaxio API commands.
 */
class Fax extends PhaxioBase {

  /**
   * Status callback.
   *
   * @var string
   */
  private $callback;

  /**
   * Batch delay (seconds).
   *
   * @var int
   */
  private $delay = 60;

  /**
   * Batch avoidance.
   *
   * @var bool
   */
  private $avoid = TRUE;

  /**
   * Cancel timeout (minutes).
   *
   * @var int
   */
  private $timeout = 3;

  /**
   * Test fail.
   *
   * @var string
   */
  private $testFail;

  /**
   * Tags.
   *
   * @var array
   */
  private $tags = [];

  /**
   * Set the callback URL.
   *
   * @param string $url
   *   A valid URL.
   */
  public function setCallback($url) {
    if (!UrlHelper::isValid($url, TRUE)) {
      throw new \Exception("URL must be a valid, absolute URL.");
    }
    $this->callback = $url;
    return $this;
  }

  /**
   * Set the batch delay.
   *
   * @param int $delay
   *   The delay in seconds.
   */
  public function setDelay($delay) {
    $this->delay = $delay;
    return $this;
  }

  /**
   * Disable batch avoidance.
   */
  public function disableAvoidance() {
    $this->avoid = FALSE;
    return $this;
  }

  /**
   * Add a tag.
   *
   * @param string $name
   *   The name of the tag.
   * @param string $value
   *   The value of the tag.
   */
  public function addTag(string $name, string $value) {
    $this->tags[$name] = $value;
    return $this;
  }

  /**
   * Trigger a test failure.
   *
   * @param string|null $failure
   *   The type of failure to trigger.
   */
  public function setTestFailure(string $failure = NULL) {
    $this->testFail = $failure ?? 'faxError';
  }

  /**
   * Send a fax.
   *
   * @param string $to
   *   The number to send the message to.
   * @param string $content
   *   The content to fax, either as a string, or as a url to a document
   */
  public function send(string $to, string $content) {
    $params = [
      'to' => $to,
      'batch_delay' => $this->delay,
      'batch_collision_avoidance' => $this->avoid,
      'cancel_timeout' => $this->timeout,
    ];

    // Add url or file content.
    if (UrlHelper::isValid($content, TRUE)) {
      $params['content_url'] = $content;
    }
    else {
      $handle = fopen($content, 'r');
      $params['file'] = $handle;
    }

    // Add callback.
    if ($this->callback) {
      $params['callback_url'] = $this->callback;
    }
    else {
      $params['callback_url'] = $GLOBALS['base_url'] . '/phaxio/status';
    }

    // Add testing.
    if ($this->testFail) {
      $params['test_fail'] = $this->testFail;
    }

    // Add tags.
    foreach ($this->tags as $name => $value) {
      $params['tag[' . $name . ']'] = $value;
    }

    // Do the send and check the result.
    $result = $this->client()->sendFax($params);
    if (isset($result->id)) {
      return $result->id;
    }
    else {
      throw new \Exception('Request to Phaxio failed.');
    }
  }

}
