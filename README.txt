# Phaxio Integration

This module provides the ability to query fax numbers and send faxes via Phaxio.

## Installation

 * Download the module
 * Enable the module
 * Add your phaxio credentials at admin/config/services/phaxio. If you have the key module installed you can use a key which is more secure than storing the credentials in regular config.

### Querying numbers

    $n = \Drupal\phaxio\Services\Numbers
      ->getAllNumbers()
      ->filterByCountry('US')
    var_export($n->numbers);
    var_export($n->details);

### Sending faxes

    $id = \Drupal\phaxio\Services\Fax
      ->send('5551234567', 'https://my.org/example.pdf');

#### Setting a webhook

    $id = \Drupal\phaxio\Services\Fax
      ->setCallback('https://my.org/phaxio')
      ->send('5551234567', 'https://my.org/example.pdf');

#### Other methods

 * setDelay() - Set the batch delay, default 60 seconds.
 * disableAvoidance() - Disable collision avoidance, enabled by default.
 * setTestFailure() - Request a simulated failure.
 * addTag() - Add a tag to the send, call multiple times for multiple tags.
